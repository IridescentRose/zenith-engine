const std = @import("std");
const log = @import("../utility/log.zig");
const event = @import("../event/event.zig");

pub fn init_base(gpa: std.mem.Allocator) !void {
    try log.init("log.txt");
    try event.init(gpa);
}

