const std = @import("std");
const Application = @import("core/Application.zig");
const State = @import("core/State.zig");
const event = @import("event/event.zig");
const platform = @import("core/platform.zig");
const log = @import("utility/log.zig");

const GameState = struct {
    fu_count: usize = 0,
    u_count: usize = 0,
    r_count: usize = 0,

    pub fn init() GameState {
        return GameState{};
    }

    pub fn deinit(self: *GameState) void {
        _ = self;
    }

    pub fn fixed_update(e: event.Event) void {
        var sctx = @as(*const State.StateContext, @ptrCast(@alignCast(e.data.ptr)));
        var self = @as(*GameState, @ptrCast(sctx.next));

        self.fu_count += 1;
        log.info("fixed_update {}", .{self.fu_count});
    }

    pub fn update(e: event.Event) void {
        var sctx = @as(*const State.StateContext, @ptrCast(@alignCast(e.data.ptr)));
        var self = @as(*GameState, @ptrCast(sctx.next));

        self.u_count += 1;
        log.info("update {}", .{self.u_count});
    }

    pub fn render(e: event.Event) void {
        var sctx = @as(*const State.StateContext, @ptrCast(@alignCast(e.data.ptr)));
        var self = @as(*GameState, @ptrCast(sctx.next));

        self.r_count += 1;
        log.info("render {}", .{self.r_count});
    }

    fn to_state(self: *GameState) State {
        _ = self;
        return State{
            .fixed_update = fixed_update,
            .update = update,
            .render = render,
        };
    }
};

var state: GameState = undefined;

fn start(self: *Application) !void {
    try self.push_state(state.to_state(), &state);
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    try platform.init_base(allocator);

    var app = try Application.init(allocator);
    app.start = start;
    state = GameState.init();

    app.run_loop();
}

