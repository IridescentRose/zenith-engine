const std = @import("std");

start: i128 = 0,
last: i128 = 0,
current: i128 = 0,

const Self = @This();

pub fn start() Self {
    var res: Self = .{};
    res.start = std.time.nanoTimestamp();
    res.last = res.start;
    res.current = res.start;

    return res;
}

pub fn reset(self: *Self) void {
    self.start = std.time.nanoTimestamp();
    self.last = self.start;
    self.current = self.start;
}

pub fn update(self: *Self) void {
    self.last = self.current;
    self.current = std.time.nanoTimestamp();
}

pub fn delta(self: *Self) f64 {
    return @as(f64, @floatFromInt(self.current - self.last)) / 1000000000.0;
}

pub fn elapsed(self: *Self) f64 {
    return @as(f64, @floatFromInt(self.current - self.start)) / 1000000000.0;
}
