const Application = @import("Application.zig");
const event = @import("../event/event.zig");

pub const StateContext = struct {
    app: *Application,
    dt: f64,
    next: *align(8) anyopaque,
};

fixed_update_idx: usize = 0,
update_idx: usize = 0,
render_idx: usize = 0,

fixed_update: event.Subscriber,
update: event.Subscriber,
render: event.Subscriber,
