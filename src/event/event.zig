const std = @import("std");

pub const EventKind = u16;
pub const Subscriber = *const fn (event: Event) void;

// Define reserved event kinds here
pub const FIXED_UPDATE: EventKind = 0;
pub const UPDATE: EventKind = 1;
pub const RENDER: EventKind = 2;

pub const SubscriberList = std.ArrayList(Subscriber);
const ChannelList = std.ArrayList(EventChannel);

pub const Event = struct {
    kind: EventKind,
    data: []const u8,
};

pub const EventChannel = struct {
    kind: EventKind,
    subscribers: SubscriberList,
};

var channels: ChannelList = undefined;
var allocator: std.mem.Allocator = undefined;
var initialized: bool = false;

pub fn init(gpa: std.mem.Allocator) !void {
    initialized = true;
    allocator = gpa;

    channels = ChannelList.init(allocator);
}

pub fn deinit() void {
    if (!initialized) return;

    for (channels.items) |*channel| {
        channel.subscribers.deinit();
    }

    channels.deinit();
}

pub fn add_channel(kind: EventKind) !void {
    try channels.append(EventChannel{
        .kind = kind,
        .subscribers = SubscriberList.init(allocator),
    });
}

pub fn subscribe(channel: EventKind, subscriber: Subscriber) !usize {
    for (channels.items) |*chan| {
        if (chan.kind == channel) {
            try chan.subscribers.append(subscriber);
            return chan.subscribers.items.len - 1;
        }
    }

    return error.ChannelNotFound;
}

pub fn unsubscribe(channel: EventKind, subscriber_id: usize) void {
    for (channels.items) |*chan| {
        if (chan.kind == channel) {
            _ = chan.subscribers.swapRemove(subscriber_id);
            return;
        }
    }
}

pub fn publish(event: Event) void {
    for (channels.items) |channel| {
        if (channel.kind == event.kind) {
            for (channel.subscribers.items) |subscriber| {
                subscriber(event);
            }
        }
    }
}
