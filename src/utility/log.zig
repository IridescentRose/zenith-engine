const std = @import("std");
const io = std.io;
const fs = std.fs;

var stdout_writer: io.BufferedWriter(4096, fs.File.Writer) = undefined;
var file_out: fs.File = undefined;
var filout_writer: io.BufferedWriter(4096, fs.File.Writer) = undefined;
var log_level: LogLevel = LogLevel.Debug;

pub fn init(filename: []const u8) !void {
    const writer = io.getStdOut().writer();
    stdout_writer = io.bufferedWriter(writer);

    file_out = try fs.cwd().createFile(filename, .{});
    const file_writer = file_out.writer();
    filout_writer = io.bufferedWriter(file_writer);
}

pub fn deinit() void {
    stdout_writer.flush() catch {}; // ignore error
    filout_writer.flush() catch {}; // ignore error
    file_out.close();
}

pub const LogLevel = enum(u8) {
    Debug = 0,
    Info = 1,
    Warning = 2,
    Error = 3,
};

pub fn set_level(level: LogLevel) void {
    log_level = level;
}

pub fn log(comptime level: LogLevel, comptime fmt: []const u8, args: anytype) void {
    if (@as(u8, @intFromEnum(level)) < @as(u8, @intFromEnum(log_level))) {
        return;
    }

    const level_str = @tagName(level);
    comptime var buf: [level_str.len]u8 = undefined;
    const level_upper = comptime std.ascii.upperString(&buf, level_str);

    const build_fmt = "\x1B[1m[" ++ level_upper ++ "]\x1B[0m " ++ fmt ++ "\n";

    stdout_writer.writer().print(build_fmt, args) catch {}; // ignore error
    filout_writer.writer().print(build_fmt, args) catch {}; // ignore error
}

pub fn dbg(comptime fmt: []const u8, args: anytype) void {
    log(LogLevel.Debug, fmt, args);
}

pub fn info(comptime fmt: []const u8, args: anytype) void {
    log(LogLevel.Info, fmt, args);
}

pub fn warn(comptime fmt: []const u8, args: anytype) void {
    log(LogLevel.Warning, fmt, args);
}

pub fn err(comptime fmt: []const u8, args: anytype) void {
    log(LogLevel.Error, fmt, args);
}

test "basic log" {
    try init("log.txt");
    defer deinit();

    info("This is a basic log test", .{});
}
