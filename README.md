# Zenith Engine

Zenith is a game engine written in Zig. It is cross-platform and takes advantage of the `-ofmt=.c` feature to automatically output cross-platform C code.

## Installation

Just use the zig code...

## Usage
## Support
## Roadmap
## Contributing
## Authors and acknowledgment

This is written by Nathan Bourgeois (Iridescence / IridescentRose)

## License

This project uses the [BSD+Patent License](https://opensource.org/license/bsdpluspatent/)

## Project status

Experimental.
