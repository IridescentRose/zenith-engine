const std = @import("std");
const State = @import("State.zig");
const Timer = @import("../utility/timer.zig");
const event = @import("../event/event.zig");

pub const AppState = struct {
    state: State,
    next: *align(8) anyopaque,
};

fixed_update_hz: f64 = 20.0,
update_hz: f64 = 144.0,
render_hz: f64 = -1.0,

allocator: std.mem.Allocator,
start: *const fn (self: *Self) anyerror!void,

state_stack: std.ArrayList(AppState),
running: bool = true,
app_timer: Timer,

const Self = @This();

var current_application: ?*Self = null;

pub fn init(gpa: std.mem.Allocator) !*Self {
    if (current_application != null)
        unreachable;

    var app = try gpa.create(Self);
    app.* = Self{
        .allocator = gpa,
        .start = undefined,
        .state_stack = std.ArrayList(AppState).init(gpa),
        .app_timer = Timer.start(),
    };
    return app;
}

pub fn quit(self: *Self) void {
    self.running = false;
}

pub fn push_state(self: *Self, state: State, next: *align(8) anyopaque) !void {
    var s = state;
    s.fixed_update_idx = try event.subscribe(event.FIXED_UPDATE, state.fixed_update);
    s.update_idx = try event.subscribe(event.UPDATE, state.update);
    s.render_idx = try event.subscribe(event.RENDER, state.render);

    try self.state_stack.append(AppState{
        .state = s,
        .next = next,
    });
}

pub fn pop_state(self: *Self) void {
    self.state_stack.pop();
}

fn pub_event(self: *Self, kind: event.EventKind, dt: f64) void {
    var ctx = State.StateContext{
        .app = self,
        .next = self.state_stack.getLast().next,
        .dt = dt,
    };

    var e = event.Event{
        .kind = kind,
        .data = std.mem.asBytes(&ctx),
    };

    event.publish(e);
}

pub fn run_loop(self: *Self) void {
    event.add_channel(event.FIXED_UPDATE) catch unreachable;
    event.add_channel(event.UPDATE) catch unreachable;
    event.add_channel(event.RENDER) catch unreachable;

    self.start(self) catch unreachable;
    self.running = true;

    var fixed_update_time = 1.0 / self.fixed_update_hz;
    var update_time = 1.0 / self.update_hz;
    var render_time = 1.0 / self.render_hz;

    self.app_timer = Timer.start();
    var fu_time: f64 = 0.0;
    var u_time: f64 = 0.0;
    var r_time: f64 = 0.0;

    while (self.running) {
        self.app_timer.update();
        fu_time += self.app_timer.delta();
        u_time += self.app_timer.delta();
        r_time += self.app_timer.delta();

        if (fu_time >= fixed_update_time) {
            self.pub_event(event.FIXED_UPDATE, fu_time);
            fu_time = 0;
        }

        if (u_time >= update_time) {
            self.pub_event(event.UPDATE, u_time);
            u_time = 0;
        }

        if (r_time >= render_time) {
            self.pub_event(event.RENDER, r_time);
            r_time = 0;
        }
    }
}
